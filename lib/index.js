'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ReactPlayerSubtitles = require('./ReactPlayerSubtitles');

var _ReactPlayerSubtitles2 = _interopRequireDefault(_ReactPlayerSubtitles);

require('./index.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_reactDom2.default.render(_react2.default.createElement(_ReactPlayerSubtitles2.default, { embed: true }), document.getElementById('root'));