'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _screenfull = require('screenfull');

var _screenfull2 = _interopRequireDefault(_screenfull);

var _reactPlayer = require('react-player');

var _reactPlayer2 = _interopRequireDefault(_reactPlayer);

var _VirtualPlayer = require('./components/VirtualPlayer');

var _VirtualPlayer2 = _interopRequireDefault(_VirtualPlayer);

var _Subtitles = require('./components/Subtitles');

var _Subtitles2 = _interopRequireDefault(_Subtitles);

var _Controls = require('./components/Controls');

var _Controls2 = _interopRequireDefault(_Controls);

var _ScriptSide = require('./components/ScriptSide');

var _ScriptSide2 = _interopRequireDefault(_ScriptSide);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReactPlayerSubtitles = function (_Component) {
  _inherits(ReactPlayerSubtitles, _Component);

  function ReactPlayerSubtitles(props) {
    _classCallCheck(this, ReactPlayerSubtitles);

    var _this = _possibleConstructorReturn(this, (ReactPlayerSubtitles.__proto__ || Object.getPrototypeOf(ReactPlayerSubtitles)).call(this, props));

    _this._onKeyDown = function (e) {
      if (e.keyCode === 32) {
        // Space
        if (_this.state.playing) {
          _this.changePlaying(false);
        } else {
          _this.changePlaying(true);
        }
        e.preventDefault();
      } else if (e.keyCode === 37) {
        // Backward
        _this.fast(-3);
      } else if (e.keyCode === 39) {
        // Forward
        _this.fast(3);
      }
    };

    _this.exitFullscreenListener = function () {
      if (!_screenfull2.default.isFullscreen) {
        _this._offFullscreen();
      }
    };

    var volume = 1;
    var subtitleDisplayType = "both";
    var subtitleIsBottom = false;
    var showTranscript = false;

    // get values from cookie if enable
    if (props.cookieEnable) {
      volume = parseFloat((0, _utils.getCookie)(props.cookiePrefix + '.volume', props.cookies)) || 1;
      subtitleDisplayType = (0, _utils.getCookie)(props.cookiePrefix + '.subtitleDisplayType', props.cookies) || "both";
      subtitleIsBottom = (0, _utils.getCookie)(props.cookiePrefix + '.subtitleIsBottom', props.cookies) === "true";
      showTranscript = (0, _utils.getCookie)(props.cookiePrefix + '.showTranscript', props.cookies) === "true";
    }

    var playerSize = _this.getPlayerSize(false, showTranscript, props.scriptSideWidth, props.playerWidth);

    _this.state = {
      duration: 0,
      currentTime: 0,
      playing: false,
      volume: volume,
      playbackRate: 1,
      qualities: ["medium"],
      quality: "medium",
      primarySubtitleData: null,
      secondarySubtitleData: null,
      primarySubtitleDataList: [],
      secondarySubtitleDataList: [],
      currentPrimarySubtitle: "",
      currentSecondarySubtitle: "",
      subtitleDisplayType: subtitleDisplayType,
      subtitleIsBottom: subtitleIsBottom,
      showTranscript: showTranscript,
      isFullscreen: false,
      displayControls: true,
      playerWidth: playerSize.playerWidth,
      playerHeight: playerSize.playerHeight
    };
    return _this;
  }

  _createClass(ReactPlayerSubtitles, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      // collect the subtitles at first time
      this.collectSubtitle(this.props.primarySubtitleURL, "primarySubtitleData");
      this.collectSubtitle(this.props.secondarySubtitleURL, "secondarySubtitleData");
      // event key down
      try {
        document.addEventListener("keydown", this._onKeyDown);
      } catch (e) {}
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      // add fullscreen event (for escape key)
      if (_screenfull2.default.enabled) {
        try {
          document.addEventListener(_screenfull2.default.raw.fullscreenchange, this.exitFullscreenListener);
        } catch (e) {}
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      // collect the subtitles if change link
      if (this.props.primarySubtitleURL !== nextProps.primarySubtitleURL) {
        this.collectSubtitle(nextProps.primarySubtitleURL, "primarySubtitleData");
      }

      if (this.props.secondarySubtitleURL !== nextProps.secondarySubtitleURL) {
        this.collectSubtitle(nextProps.secondarySubtitleURL, "secondarySubtitleData");
      }

      // change size for responsive purpose (for wrapper app)
      if (this.props.playerWidth !== nextProps.playerWidth || this.props.playerHeight !== nextProps.playerHeight) {
        var playerSize = this.getPlayerSize(false, this.props.showTranscript, nextProps.scriptSideWidth, nextProps.playerWidth);
        this.setState({
          playerHeight: playerSize.playerHeight,
          playerWidth: playerSize.playerWidth
        });
      }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //   if (nextProps.primary !== this.props.primary 
    //     || nextProps.secondary !== this.props.secondary
    //     || nextProps.playerWidth !== this.props.playerWidth) {
    //     return true;
    //   }
    //   return false;
    // }

    // componentWillUpdate(nextProps, nextState) {
    // }

  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      // turn off subtitle from youtube
      if (this.player && this.player.player.player && this.player.player.player.unloadModule) {
        this.player.player.player.unloadModule('captions');
        this.player.player.player.unloadModule('cc');
      }

      // set cookie if enable
      if (this.props.cookieEnable) {
        var cookiePrefix = this.props.cookiePrefix;
        var _state = this.state,
            volume = _state.volume,
            subtitleDisplayType = _state.subtitleDisplayType,
            subtitleIsBottom = _state.subtitleIsBottom,
            showTranscript = _state.showTranscript;

        if (volume !== prevState.volume) {
          (0, _utils.setCookie)(this.props.cookiePrefix + '.volume', volume, 60);
        }
        if (subtitleDisplayType !== prevState.subtitleDisplayType) {
          (0, _utils.setCookie)(cookiePrefix + '.subtitleDisplayType', subtitleDisplayType, 60);
        }
        if (subtitleIsBottom !== prevState.subtitleIsBottom) {
          (0, _utils.setCookie)(cookiePrefix + '.subtitleIsBottom', subtitleIsBottom, 60);
        }
        if (showTranscript !== prevState.showTranscript) {
          (0, _utils.setCookie)(cookiePrefix + '.showTranscript', showTranscript, 60);
        }
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      try {
        document.removeEventListener("keydown", this._onKeyDown);
        document.removeEventListener(_screenfull2.default.raw.fullscreenchange, this.exitFullscreenListener);
      } catch (e) {}
    }

    // _onReady() {
    //   console.log(this.player.player.player.getAvailableQualityLevels());
    // }

  }, {
    key: '_onStart',
    value: function _onStart() {
      // // count at first time play video
      // this.props.onViewCount();
      // this.lastCountTime = new Date();
      this.setState({
        qualities: this.player.player.player.getAvailableQualityLevels(),
        quality: this.player.player.player.getPlaybackQuality()
      });
    }
  }, {
    key: '_onPlay',
    value: function _onPlay() {
      this.changePlaying(true);
      this.showControls();
    }
  }, {
    key: '_onPause',
    value: function _onPause() {
      this.changePlaying(false);
      this.showControls();
    }
  }, {
    key: '_onMouseMove',
    value: function _onMouseMove() {
      this.showControls();
    }
  }, {
    key: '_onProgress',
    value: function _onProgress(current) {
      // change currentTime state
      var currentTime = current.playedSeconds;
      if (currentTime) {
        // Add 0.5s for delaying
        this.changeCurrentTime(currentTime + 0.5);
      }
    }
  }, {
    key: '_onDuration',
    value: function _onDuration(duration) {
      // change duration state
      this.setState({ duration: duration });
    }
  }, {
    key: '_onSeek',
    value: function _onSeek(time) {
      // change currentTime state and seek play
      var duration = this.state.duration;

      this.changeCurrentTime(time);
      this.player.seekTo(time / duration);
    }
  }, {
    key: '_onFullscreen',
    value: function _onFullscreen() {
      var _this2 = this;

      this.setState({ isFullscreen: true });
      if (this.props.onFullScreen) {
        this.props.onFullScreen();
      } else {
        _screenfull2.default.request((0, _reactDom.findDOMNode)(this._player));
        setTimeout(function () {
          return _this2.changePlayerSize(true, _this2.state.showTranscript);
        }, 100);
      }
    }
  }, {
    key: '_offFullscreen',
    value: function _offFullscreen() {
      this.setState({ isFullscreen: false });
      if (this.props.offFullScreen) {
        this.props.offFullScreen();
      } else {
        _screenfull2.default.exit();
        this.changePlayerSize(false, this.state.showTranscript);
      }
    }
  }, {
    key: '_onToggleTranscipt',
    value: function _onToggleTranscipt() {
      this.setState({ showTranscript: !this.state.showTranscript });
      this.changePlayerSize(this.state.isFullscreen, !this.state.showTranscript);
    }
  }, {
    key: '_onQuality',
    value: function _onQuality(quality) {
      this.setState({ quality: quality });
      this.player.player.player.setPlaybackQuality(quality);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          backgroundImage = _props.backgroundImage,
          url = _props.url,
          scriptSideWidth = _props.scriptSideWidth,
          waterMarkText = _props.waterMarkText,
          waterMarkLink = _props.waterMarkLink,
          embed = _props.embed;
      var _state2 = this.state,
          duration = _state2.duration,
          currentTime = _state2.currentTime,
          playing = _state2.playing,
          volume = _state2.volume,
          playbackRate = _state2.playbackRate,
          qualities = _state2.qualities,
          quality = _state2.quality,
          currentPrimarySubtitle = _state2.currentPrimarySubtitle,
          currentSecondarySubtitle = _state2.currentSecondarySubtitle,
          subtitleDisplayType = _state2.subtitleDisplayType,
          subtitleIsBottom = _state2.subtitleIsBottom,
          secondarySubtitleData = _state2.secondarySubtitleData,
          primarySubtitleDataList = _state2.primarySubtitleDataList,
          showTranscript = _state2.showTranscript,
          isFullscreen = _state2.isFullscreen,
          displayControls = _state2.displayControls,
          playerWidth = _state2.playerWidth,
          playerHeight = _state2.playerHeight;


      var widthVideo = playerWidth;
      var heightVideo = playerHeight;

      return _react2.default.createElement(
        'div',
        { className: 'reactPlayerSubtitles', ref: function ref(_ref) {
            return _this3._player = _ref;
          } },
        _react2.default.createElement(
          'div',
          {
            className: 'reactPlayerSubtitles__player',
            style: { height: embed ? '100vh' : heightVideo },
            onMouseMove: function onMouseMove() {
              return _this3._onMouseMove();
            } },
          _react2.default.createElement(_reactPlayer2.default, {
            ref: function ref(player) {
              return _this3.player = player;
            },
            url: url,
            width: widthVideo,
            height: heightVideo,
            playing: playing,
            volume: volume
            // onReady={() => this._onReady()}
            , playbackRate: playbackRate,
            onStart: function onStart() {
              return _this3._onStart();
            },
            onPlay: function onPlay() {
              return _this3._onPlay();
            },
            onPause: function onPause() {
              return _this3._onPause();
            },
            onProgress: function onProgress(current) {
              return _this3._onProgress(current);
            },
            onDuration: function onDuration(duration) {
              return _this3._onDuration(duration);
            }
          }),
          _react2.default.createElement(_Subtitles2.default, {
            primary: currentPrimarySubtitle,
            secondary: currentSecondarySubtitle,
            displayType: subtitleDisplayType,
            isBottom: subtitleIsBottom,
            displayControls: displayControls,
            playerWidth: playerWidth > playerHeight ? playerWidth : playerHeight
          }),
          _react2.default.createElement(_VirtualPlayer2.default, {
            onPlay: function onPlay() {
              return _this3._onPlay();
            },
            onPause: function onPause() {
              return _this3._onPause();
            },
            playing: playing,
            isStart: !currentTime,
            backgroundImage: backgroundImage,
            imageWidth: widthVideo,
            imageHeight: heightVideo,
            waterMarkText: waterMarkText,
            waterMarkLink: waterMarkLink,
            playerWidth: playerWidth > playerHeight ? playerWidth : playerHeight,
            embed: embed
          }),
          _react2.default.createElement(_Controls2.default, {
            display: displayControls,
            duration: duration,
            currentTime: currentTime,
            playing: playing,
            onSeek: function onSeek(time) {
              return _this3._onSeek(time);
            },
            onPlay: function onPlay() {
              return _this3._onPlay();
            },
            onPause: function onPause() {
              return _this3._onPause();
            },
            volume: volume,
            onVolume: function onVolume(volume) {
              return _this3.setState({ volume: volume });
            },
            onToggleTranscipt: function onToggleTranscipt() {
              return _this3._onToggleTranscipt();
            },
            onFullscreen: function onFullscreen() {
              return _this3._onFullscreen();
            },
            offFullscreen: function offFullscreen() {
              return _this3._offFullscreen();
            },
            isFullscreen: isFullscreen,
            onMouseEnter: function onMouseEnter() {
              return _this3.setState({ mouseOnControls: true });
            },
            onMouseLeave: function onMouseLeave() {
              return _this3.setState({ mouseOnControls: false });
            },
            subtitleDisplayType: subtitleDisplayType,
            subtitleIsBottom: subtitleIsBottom,
            changeDisplayType: function changeDisplayType(type) {
              return _this3.setState({ subtitleDisplayType: type });
            },
            changeIsBottom: function changeIsBottom(bool) {
              return _this3.setState({ subtitleIsBottom: bool });
            },
            playbackRate: playbackRate,
            onPlaybackRate: function onPlaybackRate(playbackRate) {
              return _this3.setState({ playbackRate: playbackRate });
            },
            qualities: qualities,
            quality: quality,
            onQuality: function onQuality(quality) {
              return _this3._onQuality(quality);
            }
          })
        ),
        showTranscript && typeof window !== 'undefined' && window.innerWidth > 768 ? _react2.default.createElement(_ScriptSide2.default, {
          height: playerHeight,
          width: scriptSideWidth
          // primaryData={primarySubtitleData}
          , primaryDataList: primarySubtitleDataList,
          secondaryData: secondarySubtitleData
          // secondaryDataList={secondarySubtitleDataList}
          , onClick: function onClick(time) {
            return _this3._onSeek(time);
          },
          currentPrimary: currentPrimarySubtitle
        }) : null
      );
    }
  }, {
    key: 'collectSubtitle',
    value: function collectSubtitle(url, stateName) {
      var _this4 = this;

      (0, _utils.getTextFileContent)(url, function (text) {
        var _this4$setState;

        var subtitleData = (0, _utils.textContentToSubtitleData)(text);
        _this4.setState((_this4$setState = {}, _defineProperty(_this4$setState, stateName, subtitleData.data), _defineProperty(_this4$setState, stateName + 'List', subtitleData.list), _this4$setState));
      });
    }
  }, {
    key: 'changeCurrentTime',
    value: function changeCurrentTime(time) {
      // add change the current subtitles too for performance require
      var _state3 = this.state,
          primarySubtitleDataList = _state3.primarySubtitleDataList,
          secondarySubtitleDataList = _state3.secondarySubtitleDataList;

      this.setState({
        currentTime: time,
        currentPrimarySubtitle: (0, _utils.getCurrentSubtitle)(time, primarySubtitleDataList),
        currentSecondarySubtitle: (0, _utils.getCurrentSubtitle)(time, secondarySubtitleDataList)
      });
    }
  }, {
    key: 'changePlaying',
    value: function changePlaying(state) {
      // count video view if reach the conditions
      if (state) {
        var currentPercent = this.state.currentTime / (this.state.duration / 100);
        if (!this.lastCountTime || new Date() - this.lastCountTime > this.props.viewCountTime * 60000 && currentPercent < 2) {
          this.props.onViewCount();
          this.lastCountTime = new Date();
        }
      }

      // change playing state
      this.setState({ playing: state });
    }
  }, {
    key: 'showControls',
    value: function showControls() {
      var _this5 = this;

      // show controls on 3s
      if (!this.state.displayControls) {
        this.setState({ displayControls: true });
      }
      clearTimeout(this._timeout);
      if (this.state.playing) {
        this._timeout = setTimeout(function () {
          return _this5.setState({
            displayControls: !_this5.state.playing || _this5.state.mouseOnControls ? true : false
          });
        }, 800);
      }
    }
  }, {
    key: 'fast',
    value: function fast(time) {
      this._onSeek(this.state.currentTime + time);
    }
  }, {
    key: 'getPlayerSize',
    value: function getPlayerSize(isFullscreen, showTranscript, scriptSideWidth, playerWidthInput) {
      var playerWidth = playerWidthInput;
      if (typeof window !== 'undefined') {
        if (showTranscript && window.innerWidth > 768 && playerWidth + scriptSideWidth > window.innerWidth) {
          playerWidth = window.innerWidth - scriptSideWidth;
        } else if (playerWidth > window.innerWidth) {
          playerWidth = window.innerWidth;
        }
      }

      var playerHeight = playerWidth * 480 / 853;
      if (typeof window !== 'undefined') {
        if (showTranscript && window.innerWidth > 768 && playerHeight < 350) {
          playerHeight = 350;
        }
      }

      if (typeof window !== 'undefined') {
        if (isFullscreen || this.props.embed) {
          playerWidth = window.innerWidth;
          playerHeight = window.innerHeight;
          if (showTranscript && window.innerWidth > 768) {
            playerWidth = window.innerWidth - scriptSideWidth;
          }
        }
      }

      return { playerHeight: playerHeight, playerWidth: playerWidth };
    }
  }, {
    key: 'changePlayerSize',
    value: function changePlayerSize(isFullscreen, showTranscript) {
      var playerSize = this.getPlayerSize(isFullscreen, showTranscript, this.props.scriptSideWidth, this.props.playerWidth);
      this.setState({
        playerHeight: playerSize.playerHeight,
        playerWidth: playerSize.playerWidth
      });
    }
  }]);

  return ReactPlayerSubtitles;
}(_react.Component);

ReactPlayerSubtitles.propTypes = {
  backgroundImage: _propTypes2.default.string,
  url: _propTypes2.default.string,
  playerWidth: _propTypes2.default.number,
  scriptSideWidth: _propTypes2.default.number,
  primarySubtitleURL: _propTypes2.default.string,
  secondarySubtitleURL: _propTypes2.default.string,
  primarySubtitleName: _propTypes2.default.string,
  secondarySubtitleName: _propTypes2.default.string,
  onViewCount: _propTypes2.default.func,
  viewCountTime: _propTypes2.default.number,
  onFullScreen: _propTypes2.default.func,
  offFullScreen: _propTypes2.default.func,
  embed: _propTypes2.default.bool,
  waterMarkText: _propTypes2.default.string,
  waterMarkLink: _propTypes2.default.string
};

ReactPlayerSubtitles.defaultProps = {
  backgroundImage: "https://sosub.org/media/video_images/image_lhQ4UY5.jpg",
  url: "https://www.youtube.com/watch?v=uFX95HahaUs",
  // playerWidth: '100%',
  playerWidth: 853,
  scriptSideWidth: 340,
  primarySubtitleURL: "/vi_Sg5M9yX_HA4qmn3.srt",
  secondarySubtitleURL: "/en_GkAJOX2_B9gswf1.srt",
  primarySubtitleName: "Tiếng Việt",
  secondarySubtitleName: "English",
  onViewCount: function onViewCount() {
    return console.log("onViewCount");
  },
  viewCountTime: 3,
  cookiePrefix: "SOSUB",
  cookieEnable: true,
  embed: false,
  waterMarkText: "SOSUB.org",
  waterMarkLink: "SOSUB.org"
};

exports.default = ReactPlayerSubtitles;