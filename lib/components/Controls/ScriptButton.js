"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScriptButton = function (_Component) {
  _inherits(ScriptButton, _Component);

  function ScriptButton() {
    _classCallCheck(this, ScriptButton);

    return _possibleConstructorReturn(this, (ScriptButton.__proto__ || Object.getPrototypeOf(ScriptButton)).apply(this, arguments));
  }

  _createClass(ScriptButton, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      if (nextProps.onClick !== this.props.onClick) {
        return true;
      }
      return false;
    }
  }, {
    key: "render",
    value: function render() {
      var _onClick = this.props.onClick;


      return _react2.default.createElement(
        "button",
        { onClick: function onClick() {
            return _onClick();
          } },
        _react2.default.createElement(
          "svg",
          { x: "0px", y: "0px", width: "20", height: "20", fill: "#E33734", viewBox: "0 0 512 512" },
          _react2.default.createElement(
            "g",
            null,
            _react2.default.createElement("path", { d: "M64,64v384h384V64H64z M144,368c-8.836,0-16-7.164-16-16s7.164-16,16-16s16,7.164,16,16S152.836,368,144,368z M144,272 c-8.836,0-16-7.164-16-16s7.164-16,16-16s16,7.164,16,16S152.836,272,144,272z M144,176c-8.836,0-16-7.164-16-16s7.164-16,16-16 s16,7.164,16,16S152.836,176,144,176z M384,360H192v-16h192V360z M384,264H192v-16h192V264z M384,168H192v-16h192V168z" })
          )
        )
      );
    }
  }]);

  return ScriptButton;
}(_react.Component);

exports.default = ScriptButton;