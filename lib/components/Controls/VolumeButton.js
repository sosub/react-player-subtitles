'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _VolumeSlider = require('./VolumeSlider');

var _VolumeSlider2 = _interopRequireDefault(_VolumeSlider);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VolumeButton = function (_Component) {
  _inherits(VolumeButton, _Component);

  function VolumeButton(props) {
    _classCallCheck(this, VolumeButton);

    var _this = _possibleConstructorReturn(this, (VolumeButton.__proto__ || Object.getPrototypeOf(VolumeButton)).call(this, props));

    _this.state = {
      isMute: props.volume === 0,
      volume: props.volume,
      showVolumeSlider: false
    };
    return _this;
  }

  _createClass(VolumeButton, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextState.isMute !== this.state.isMute || nextState.volume !== this.state.volume || nextState.showVolumeSlider !== this.state.showVolumeSlider) {
        return true;
      }
      return false;
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.volume !== this.props.volume) {
        this.setState({ isMute: nextProps.volume === 0 });
        if (nextProps.volume !== 0) {
          this.setState({ volume: nextProps.volume });
        }
      }
    }
  }, {
    key: '_on',
    value: function _on() {
      this.props.onVolume(this.state.volume);
    }
  }, {
    key: '_off',
    value: function _off() {
      this.props.onVolume(0);
    }
  }, {
    key: '_onMouseEnter',
    value: function _onMouseEnter() {
      this.setState({ showVolumeSlider: true });
    }
  }, {
    key: '_onMouseLeave',
    value: function _onMouseLeave() {
      this.setState({ showVolumeSlider: false });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _state = this.state,
          isMute = _state.isMute,
          volume = _state.volume,
          showVolumeSlider = _state.showVolumeSlider;
      var onVolume = this.props.onVolume;

      return _react2.default.createElement(
        'div',
        { className: 'volumeButton', onMouseEnter: function onMouseEnter() {
            return _this2._onMouseEnter();
          }, onMouseLeave: function onMouseLeave() {
            return _this2._onMouseLeave();
          } },
        isMute ? _react2.default.createElement(
          'button',
          { onClick: function onClick() {
              return _this2._on();
            } },
          _react2.default.createElement(
            'svg',
            { x: '0px', y: '0px', width: '20', height: '20', fill: 'red', viewBox: '0 0 512 512' },
            _react2.default.createElement(
              'g',
              null,
              _react2.default.createElement('path', { d: 'M405.5,256c0,22.717-4.883,44.362-13.603,63.855l31.88,31.88C439.283,323.33,448,290.653,448,256 c0-93.256-64-172.254-149-192v44.978C361,127.632,405.5,186.882,405.5,256z' }),
              _react2.default.createElement('polygon', { points: '256,80.458 204.979,132.938 256,183.957   ' }),
              _react2.default.createElement('path', { d: 'M420.842,396.885L91.116,67.157l-24,24l90.499,90.413l-8.28,10.43H64v128h85.334L256,431.543V280l94.915,94.686 C335.795,387.443,318,397.213,299,403.022V448c31-7.172,58.996-22.163,82.315-42.809l39.61,39.693l24-24.043l-24.002-24.039 L420.842,396.885z' }),
              _react2.default.createElement('path', { d: 'M352.188,256c0-38.399-21.188-72.407-53.188-88.863v59.82l50.801,50.801C351.355,270.739,352.188,263.454,352.188,256z' })
            )
          )
        ) : volume > 0.5 ? _react2.default.createElement(
          'button',
          { onClick: function onClick() {
              return _this2._off();
            } },
          _react2.default.createElement(
            'svg',
            { x: '0px', y: '0px', width: '20', height: '20', fill: 'red', viewBox: '0 0 512 512' },
            _react2.default.createElement('path', { d: 'M64,192v128h85.334L256,431.543V80.458L149.334,192H64z M352,256c0-38.399-21.333-72.407-53.333-88.863v176.636 C330.667,328.408,352,294.4,352,256z M298.667,64v44.978C360.531,127.632,405.334,186.882,405.334,256 c0,69.119-44.803,128.369-106.667,147.022V448C384,428.254,448,349.257,448,256C448,162.744,384,83.746,298.667,64z' })
          )
        ) : _react2.default.createElement(
          'button',
          { onClick: function onClick() {
              return _this2._off();
            } },
          _react2.default.createElement(
            'svg',
            { x: '0px', y: '0px', width: '20', height: '20', fill: 'red', viewBox: '0 0 512 512' },
            _react2.default.createElement('path', { d: 'M64,192v128h85.334L256,431.543V80.458L149.334,192H64z M352,256c0-38.399-21.333-72.407-53.333-88.863v176.636 C330.667,328.408,352,294.4,352,256z' })
          )
        ),
        showVolumeSlider ? _react2.default.createElement(_VolumeSlider2.default, {
          volume: volume,
          onChange: function onChange(volume) {
            return onVolume(volume);
          }
        }) : null
      );
    }
  }]);

  return VolumeButton;
}(_react.Component);

exports.default = VolumeButton;