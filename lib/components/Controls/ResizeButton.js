"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResizeButton = function (_Component) {
  _inherits(ResizeButton, _Component);

  function ResizeButton() {
    _classCallCheck(this, ResizeButton);

    return _possibleConstructorReturn(this, (ResizeButton.__proto__ || Object.getPrototypeOf(ResizeButton)).apply(this, arguments));
  }

  _createClass(ResizeButton, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      if (nextProps.isFullscreen !== this.props.isFullscreen) {
        return true;
      }
      return false;
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          onFullscreen = _props.onFullscreen,
          offFullscreen = _props.offFullscreen,
          isFullscreen = _props.isFullscreen;


      return isFullscreen ? _react2.default.createElement(
        "button",
        { className: "resizeButton", onClick: function onClick() {
            return offFullscreen();
          } },
        _react2.default.createElement(
          "svg",
          { x: "0px", y: "0px", width: "20", height: "20", fill: "#E33734", viewBox: "0 0 512 512" },
          _react2.default.createElement(
            "g",
            { id: "check-box-outline-blank" },
            _react2.default.createElement("path", { d: "M408,51v357H51V51H408 M408,0H51C22.95,0,0,22.95,0,51v357c0,28.05,22.95,51,51,51h357c28.05,0,51-22.95,51-51V51 C459,22.95,436.05,0,408,0L408,0z" })
          )
        )
      ) : _react2.default.createElement(
        "button",
        { className: "resizeButton", onClick: function onClick() {
            return onFullscreen();
          } },
        _react2.default.createElement(
          "svg",
          { x: "0px", y: "0px", width: "20", height: "20", fill: "#E33734", viewBox: "0 0 512 512" },
          _react2.default.createElement("polygon", { points: "288,96 337.9,145.9 274,209.7 274,209.7 145.9,337.9 96,288 96,416 224,416 174.1,366.1 357.4,182.9 366.1,174.1  416,224 416,96 " })
        )
      );
    }
  }]);

  return ResizeButton;
}(_react.Component);

exports.default = ResizeButton;