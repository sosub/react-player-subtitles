'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TwoLayersMenu = require('./TwoLayersMenu');

var _TwoLayersMenu2 = _interopRequireDefault(_TwoLayersMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SettingsButton = function (_Component) {
  _inherits(SettingsButton, _Component);

  function SettingsButton(props) {
    _classCallCheck(this, SettingsButton);

    var _this = _possibleConstructorReturn(this, (SettingsButton.__proto__ || Object.getPrototypeOf(SettingsButton)).call(this, props));

    _this.listenerHideMenu = function (e) {
      if (e.path.indexOf(_this._ref) === -1) {
        _this.hideMenu();
      }
    };

    _this.state = {
      displayMenu: false
    };
    return _this;
  }

  _createClass(SettingsButton, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.playbackRate !== this.props.playbackRate || nextProps.quality !== this.props.quality || nextProps.qualities !== this.props.qualities || nextState.displayMenu !== this.state.displayMenu) {
        return true;
      }
      return false;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      document.removeEventListener("click", this.listenerHideMenu);
    }
  }, {
    key: '_onClick',
    value: function _onClick() {
      if (this.state.displayMenu) {
        this.hideMenu();
      } else {
        this.showMenu();
      }
    }
  }, {
    key: 'showMenu',
    value: function showMenu() {
      this.setState({ displayMenu: true });
      document.addEventListener("click", this.listenerHideMenu);
    }
  }, {
    key: 'hideMenu',
    value: function hideMenu() {
      this.setState({ displayMenu: false });
      document.removeEventListener("click", this.listenerHideMenu);
    }
  }, {
    key: 'convertQualityText',
    value: function convertQualityText(quality) {
      var qualityText = quality;
      if (quality === "auto") {
        qualityText = "Tự động";
      } else if (quality === "tiny") {
        qualityText = "144p";
      } else if (quality === "small") {
        qualityText = "240p";
      } else if (quality === "medium") {
        qualityText = "360p";
      } else if (quality === "large") {
        qualityText = "480p";
      } else if (quality === "hd720") {
        qualityText = "720p";
      } else if (quality === "hd1080") {
        qualityText = "1080p";
      }
      return qualityText;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          playbackRate = _props.playbackRate,
          onPlaybackRate = _props.onPlaybackRate,
          quality = _props.quality,
          qualities = _props.qualities,
          onQuality = _props.onQuality;


      var menuData = [{
        label: "Tốc độ",
        value: playbackRate.toString(),
        items: [{
          value: "0.25",
          action: function action() {
            return onPlaybackRate(0.25);
          }
        }, {
          value: "0.5",
          action: function action() {
            return onPlaybackRate(0.5);
          }
        }, {
          value: "0.75",
          action: function action() {
            return onPlaybackRate(0.75);
          }
        }, {
          value: "1",
          action: function action() {
            return onPlaybackRate(1);
          }
        }, {
          value: "1.25",
          action: function action() {
            return onPlaybackRate(1.25);
          }
        }, {
          value: "1.5",
          action: function action() {
            return onPlaybackRate(1.5);
          }
        }]
      }, {
        label: "Chất lượng",
        value: this.convertQualityText(quality),
        items: qualities.map(function (quality) {
          return {
            value: _this2.convertQualityText(quality),
            action: function action() {
              return onQuality(quality);
            }
          };
        })
      }];

      return _react2.default.createElement(
        'div',
        { className: 'captionButton', ref: function ref(r) {
            return _this2._ref = r;
          } },
        _react2.default.createElement(
          'button',
          { onClick: function onClick() {
              return _this2._onClick();
            } },
          _react2.default.createElement(
            'svg',
            { x: '0px', y: '0px', width: '20', height: '20', fill: '#E33734', viewBox: '0 0 512 512' },
            _react2.default.createElement('path', { d: 'M413.967,276.8c1.06-6.235,1.06-13.518,1.06-20.8s-1.06-13.518-1.06-20.8l44.667-34.318 c4.26-3.118,5.319-8.317,2.13-13.518L418.215,115.6c-2.129-4.164-8.507-6.235-12.767-4.164l-53.186,20.801 c-10.638-8.318-23.394-15.601-36.16-20.801l-7.448-55.117c-1.06-4.154-5.319-8.318-10.638-8.318h-85.098 c-5.318,0-9.577,4.164-10.637,8.318l-8.508,55.117c-12.767,5.2-24.464,12.482-36.171,20.801l-53.186-20.801 c-5.319-2.071-10.638,0-12.767,4.164l-42.549,71.765c-2.119,4.153-1.061,10.399,2.129,13.518L96.97,235.2 c0,7.282-1.06,13.518-1.06,20.8s1.06,13.518,1.06,20.8l-44.668,34.318c-4.26,3.118-5.318,8.317-2.13,13.518L92.721,396.4 c2.13,4.164,8.508,6.235,12.767,4.164l53.187-20.801c10.637,8.318,23.394,15.601,36.16,20.801l8.508,55.117 c1.069,5.2,5.318,8.318,10.637,8.318h85.098c5.319,0,9.578-4.164,10.638-8.318l8.518-55.117c12.757-5.2,24.464-12.482,36.16-20.801 l53.187,20.801c5.318,2.071,10.637,0,12.767-4.164l42.549-71.765c2.129-4.153,1.06-10.399-2.13-13.518L413.967,276.8z M255.468,328.8c-41.489,0-74.46-32.235-74.46-72.8s32.971-72.8,74.46-72.8s74.461,32.235,74.461,72.8S296.957,328.8,255.468,328.8 z' })
          )
        ),
        this.state.displayMenu && _react2.default.createElement(_TwoLayersMenu2.default, {
          data: menuData
        })
      );
    }
  }]);

  return SettingsButton;
}(_react.Component);

exports.default = SettingsButton;