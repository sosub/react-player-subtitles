"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TwoLayersMenu = function (_Component) {
  _inherits(TwoLayersMenu, _Component);

  function TwoLayersMenu(props) {
    _classCallCheck(this, TwoLayersMenu);

    var _this = _possibleConstructorReturn(this, (TwoLayersMenu.__proto__ || Object.getPrototypeOf(TwoLayersMenu)).call(this, props));

    _this.state = {
      displayLayer: null
    };
    return _this;
  }

  _createClass(TwoLayersMenu, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.data !== this.props.data || nextState.displayLayer !== this.state.displayLayer) {
        return true;
      }
      return false;
    }
  }, {
    key: "_onClick",
    value: function _onClick(layer) {
      this.setState({ displayLayer: layer });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var data = this.props.data;
      var displayLayer = this.state.displayLayer;

      return _react2.default.createElement(
        "table",
        { className: "twoLayersMenu" },
        displayLayer === null ? _react2.default.createElement(
          "tbody",
          null,
          data.map(function (layer, idx) {
            return _react2.default.createElement(
              "tr",
              { key: idx, onClick: function onClick() {
                  return _this2._onClick(layer);
                } },
              _react2.default.createElement(
                "td",
                null,
                _react2.default.createElement(
                  "label",
                  null,
                  layer.label
                )
              ),
              _react2.default.createElement(
                "td",
                null,
                _react2.default.createElement(
                  "b",
                  null,
                  layer.value
                )
              ),
              _react2.default.createElement(
                "td",
                null,
                _react2.default.createElement(
                  "svg",
                  { x: "0px", y: "0px", width: "14", height: "14", fill: "#1c2029", viewBox: "0 0 512 512" },
                  _react2.default.createElement("path", { d: "M298.3,256L298.3,256L298.3,256L131.1,81.9c-4.2-4.3-4.1-11.4,0.2-15.8l29.9-30.6c4.3-4.4,11.3-4.5,15.5-0.2l204.2,212.7 c2.2,2.2,3.2,5.2,3,8.1c0.1,3-0.9,5.9-3,8.1L176.7,476.8c-4.2,4.3-11.2,4.2-15.5-0.2L131.3,446c-4.3-4.4-4.4-11.5-0.2-15.8 L298.3,256z" })
                )
              )
            );
          })
        ) : _react2.default.createElement(
          "tbody",
          null,
          _react2.default.createElement(
            "tr",
            { onClick: function onClick() {
                return _this2._onClick(null);
              } },
            _react2.default.createElement(
              "td",
              null,
              _react2.default.createElement(
                "svg",
                { x: "0px", y: "0px", width: "12", height: "12", fill: "#6e727d", viewBox: "0 0 512 512" },
                _react2.default.createElement("path", { d: "M213.7,256L213.7,256L213.7,256L380.9,81.9c4.2-4.3,4.1-11.4-0.2-15.8l-29.9-30.6c-4.3-4.4-11.3-4.5-15.5-0.2L131.1,247.9 c-2.2,2.2-3.2,5.2-3,8.1c-0.1,3,0.9,5.9,3,8.1l204.2,212.7c4.2,4.3,11.2,4.2,15.5-0.2l29.9-30.6c4.3-4.4,4.4-11.5,0.2-15.8 L213.7,256z" })
              )
            ),
            _react2.default.createElement(
              "td",
              null,
              _react2.default.createElement(
                "label",
                null,
                "Tr\u1EDF l\u1EA1i ",
                displayLayer.label
              )
            )
          ),
          displayLayer.items.map(function (item, idx) {
            return _react2.default.createElement(
              "tr",
              {
                key: idx,
                onClick: function onClick() {
                  item.action();_this2._onClick(null);
                },
                className: item.value === displayLayer.value ? "active" : null },
              _react2.default.createElement(
                "td",
                null,
                _react2.default.createElement("label", null)
              ),
              _react2.default.createElement(
                "td",
                null,
                _react2.default.createElement(
                  "b",
                  null,
                  item.value
                )
              )
            );
          })
        )
      );
    }
  }]);

  return TwoLayersMenu;
}(_react.Component);

exports.default = TwoLayersMenu;