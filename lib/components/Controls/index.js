'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ProgressBar = require('./ProgressBar');

var _ProgressBar2 = _interopRequireDefault(_ProgressBar);

var _PlayAndPause = require('./PlayAndPause');

var _PlayAndPause2 = _interopRequireDefault(_PlayAndPause);

var _VolumeButton = require('./VolumeButton');

var _VolumeButton2 = _interopRequireDefault(_VolumeButton);

var _ScriptButton = require('./ScriptButton');

var _ScriptButton2 = _interopRequireDefault(_ScriptButton);

var _Duration = require('./Duration');

var _Duration2 = _interopRequireDefault(_Duration);

var _CaptionButton = require('./CaptionButton');

var _CaptionButton2 = _interopRequireDefault(_CaptionButton);

var _ResizeButton = require('./ResizeButton');

var _ResizeButton2 = _interopRequireDefault(_ResizeButton);

var _SettingsButton = require('./SettingsButton');

var _SettingsButton2 = _interopRequireDefault(_SettingsButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Controls = function (_Component) {
  _inherits(Controls, _Component);

  function Controls() {
    _classCallCheck(this, Controls);

    return _possibleConstructorReturn(this, (Controls.__proto__ || Object.getPrototypeOf(Controls)).apply(this, arguments));
  }

  _createClass(Controls, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          display = _props.display,
          _onMouseEnter = _props.onMouseEnter,
          _onMouseLeave = _props.onMouseLeave,
          duration = _props.duration,
          currentTime = _props.currentTime,
          onSeek = _props.onSeek,
          playing = _props.playing,
          _onPlay = _props.onPlay,
          _onPause = _props.onPause,
          volume = _props.volume,
          _onVolume = _props.onVolume,
          onToggleTranscipt = _props.onToggleTranscipt,
          _onFullscreen = _props.onFullscreen,
          _offFullscreen = _props.offFullscreen,
          isFullscreen = _props.isFullscreen,
          subtitleDisplayType = _props.subtitleDisplayType,
          subtitleIsBottom = _props.subtitleIsBottom,
          _changeDisplayType = _props.changeDisplayType,
          _changeIsBottom = _props.changeIsBottom,
          playbackRate = _props.playbackRate,
          _onPlaybackRate = _props.onPlaybackRate,
          quality = _props.quality,
          qualities = _props.qualities,
          _onQuality = _props.onQuality;


      return _react2.default.createElement(
        'div',
        {
          className: 'controls' + (!display ? ' hidden' : ''),
          onMouseEnter: function onMouseEnter() {
            return _onMouseEnter();
          },
          onMouseLeave: function onMouseLeave() {
            return _onMouseLeave();
          } },
        _react2.default.createElement(_ProgressBar2.default, {
          duration: duration,
          currentTime: currentTime,
          onChange: function onChange(time) {
            return onSeek(time);
          }
        }),
        _react2.default.createElement(
          'div',
          { className: 'items' },
          _react2.default.createElement(
            'div',
            { className: 'leftItems' },
            _react2.default.createElement(_PlayAndPause2.default, {
              playing: playing,
              onPlay: function onPlay() {
                return _onPlay();
              },
              onPause: function onPause() {
                return _onPause();
              }
            }),
            _react2.default.createElement(_VolumeButton2.default, {
              volume: volume,
              onVolume: function onVolume(volume) {
                return _onVolume(volume);
              }
            }),
            _react2.default.createElement(_Duration2.default, {
              current: currentTime,
              duration: duration
            })
          ),
          _react2.default.createElement(
            'div',
            { className: 'rightItems' },
            typeof window !== 'undefined' && window.innerWidth > 768 && _react2.default.createElement(_SettingsButton2.default, {
              playbackRate: playbackRate,
              onPlaybackRate: function onPlaybackRate(rate) {
                return _onPlaybackRate(rate);
              },
              quality: quality,
              qualities: qualities,
              onQuality: function onQuality(quality) {
                return _onQuality(quality);
              }
            }),
            _react2.default.createElement(_CaptionButton2.default, {
              subtitleDisplayType: subtitleDisplayType,
              subtitleIsBottom: subtitleIsBottom,
              changeDisplayType: function changeDisplayType(type) {
                return _changeDisplayType(type);
              },
              changeIsBottom: function changeIsBottom(bool) {
                return _changeIsBottom(bool);
              }
            }),
            typeof window !== 'undefined' && window.innerWidth > 768 && _react2.default.createElement(_ScriptButton2.default, {
              onClick: function onClick() {
                return onToggleTranscipt();
              }
            }),
            _react2.default.createElement(_ResizeButton2.default, {
              onFullscreen: function onFullscreen() {
                return _onFullscreen();
              },
              offFullscreen: function offFullscreen() {
                return _offFullscreen();
              },
              isFullscreen: isFullscreen
            })
          )
        )
      );
    }
  }]);

  return Controls;
}(_react.Component);

exports.default = Controls;