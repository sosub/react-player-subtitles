"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Subtitles = function (_Component) {
  _inherits(Subtitles, _Component);

  function Subtitles() {
    _classCallCheck(this, Subtitles);

    return _possibleConstructorReturn(this, (Subtitles.__proto__ || Object.getPrototypeOf(Subtitles)).apply(this, arguments));
  }

  _createClass(Subtitles, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.primary !== this.props.primary || nextProps.secondary !== this.props.secondary || nextProps.displayType !== this.props.displayType || nextProps.isBottom !== this.props.isBottom || nextProps.displayControls !== this.props.displayControls || nextProps.playerWidth !== this.props.playerWidth) {
        return true;
      }
      return false;
    }
  }, {
    key: "renderPrimaryType",
    value: function renderPrimaryType(text) {
      var fontSize = this.props.playerWidth * 22 / 853;
      return text && _react2.default.createElement(
        "div",
        { className: "primary" },
        _react2.default.createElement(
          "span",
          { style: { fontSize: fontSize } },
          text
        )
      );
    }
  }, {
    key: "renderSecondaryType",
    value: function renderSecondaryType(text) {
      var fontSize = this.props.playerWidth * 18 / 853;
      return text && _react2.default.createElement(
        "div",
        { className: "secondary" },
        _react2.default.createElement(
          "span",
          { style: { fontSize: fontSize } },
          text
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          primary = _props.primary,
          secondary = _props.secondary,
          displayType = _props.displayType,
          isBottom = _props.isBottom,
          displayControls = _props.displayControls;


      return displayType !== "off" ? _react2.default.createElement(
        "div",
        { className: "subtitles", style: { bottom: displayControls ? 60 : 10 } },
        displayType === "both" && !isBottom ? _react2.default.createElement(
          "div",
          { className: "top" },
          this.renderPrimaryType(secondary)
        ) : null,
        displayType === "both" ? _react2.default.createElement(
          "div",
          { className: "bottom" },
          this.renderPrimaryType(primary),
          isBottom && this.renderSecondaryType(secondary)
        ) : _react2.default.createElement(
          "div",
          { className: "bottom" },
          this.renderPrimaryType(displayType === "primary" ? primary : secondary)
        )
      ) : null;
    }
  }]);

  return Subtitles;
}(_react.Component);

exports.default = Subtitles;