'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScriptSide = function (_Component) {
  _inherits(ScriptSide, _Component);

  function ScriptSide(props) {
    _classCallCheck(this, ScriptSide);

    var _this = _possibleConstructorReturn(this, (ScriptSide.__proto__ || Object.getPrototypeOf(ScriptSide)).call(this, props));

    _this.state = {
      onMouse: false
    };
    return _this;
  }

  _createClass(ScriptSide, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      setTimeout(function () {
        _this2.scrollSubtitle(_this2.props.currentPrimary);
      }, 500);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextState.onMouse !== this.state.onMouse || nextProps.height !== this.props.height || nextProps.width !== this.props.width || nextProps.secondaryData !== this.props.secondaryData || nextProps.primaryDataList !== this.props.primaryDataList || nextProps.currentPrimary !== this.props.currentPrimary) {
        return true;
      }
      return false;
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.currentPrimary !== this.props.currentPrimary && !this.state.onMouse) {
        this.scrollSubtitle(nextProps.currentPrimary);
      }
    }
  }, {
    key: '_onMouseEnter',
    value: function _onMouseEnter() {
      this.setState({ onMouse: true });
    }
  }, {
    key: '_onMouseLeave',
    value: function _onMouseLeave() {
      this.scrollSubtitle(this.props.currentPrimary);
      this.setState({ onMouse: false });
    }
  }, {
    key: '_onMouseMove',
    value: function _onMouseMove() {
      if (!this.state.onMouse) {
        this.setState({ onMouse: true });
      }
    }
  }, {
    key: 'scrollSubtitle',
    value: function scrollSubtitle(currentSubtitle) {
      try {
        var currentSubtitleNumber = this.props.primaryDataList.map(function (subtitle) {
          return subtitle.content;
        }).indexOf(currentSubtitle);
        var ul = document.getElementsByClassName('scriptSide__ul')[0];
        var ulOffsetTop = ul.offsetTop;
        var ulHeight = ul.offsetHeight;
        var currentLi = document.getElementsByClassName('scriptSide__ul__li')[currentSubtitleNumber - 1];
        var liOffsetTop = currentLi.offsetTop;
        var liHeight = currentLi.offsetHeight;
        document.getElementsByClassName('scriptSide__ul')[0].scrollTop = liOffsetTop - ulOffsetTop + liHeight - ulHeight / 2 + liHeight / 2;
      } catch (e) {}
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          height = _props.height,
          width = _props.width,
          secondaryData = _props.secondaryData,
          primaryDataList = _props.primaryDataList,
          _onClick = _props.onClick,
          currentPrimary = _props.currentPrimary;

      return _react2.default.createElement(
        'div',
        { className: 'scriptSide', style: { height: height, width: width } },
        _react2.default.createElement(
          'ul',
          {
            className: 'scriptSide__ul',
            onMouseEnter: function onMouseEnter() {
              return _this3._onMouseEnter();
            },
            onMouseLeave: function onMouseLeave() {
              return _this3._onMouseLeave();
            },
            onMouseMove: function onMouseMove() {
              return _this3._onMouseMove();
            } },
          primaryDataList.map(function (subtitle) {
            return _react2.default.createElement(
              'li',
              {
                key: subtitle.key,
                className: 'scriptSide__ul__li ' + (currentPrimary === subtitle.content ? "active" : ""),
                onClick: function onClick() {
                  return _onClick(subtitle.begin + 0.1);
                } },
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'span',
                  null,
                  (0, _utils.secondsToTimeString)(subtitle.begin)
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'p',
                    { className: 'primarySubtitle' },
                    subtitle.content
                  ),
                  _react2.default.createElement(
                    'p',
                    { className: 'secondarySubtitle' },
                    secondaryData && secondaryData[subtitle.key] ? secondaryData[subtitle.key].content : ""
                  )
                )
              )
            );
          })
        )
      );
    }
  }]);

  return ScriptSide;
}(_react.Component);

exports.default = ScriptSide;