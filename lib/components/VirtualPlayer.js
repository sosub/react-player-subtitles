"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VirtualPlayer = function (_Component) {
  _inherits(VirtualPlayer, _Component);

  function VirtualPlayer() {
    _classCallCheck(this, VirtualPlayer);

    return _possibleConstructorReturn(this, (VirtualPlayer.__proto__ || Object.getPrototypeOf(VirtualPlayer)).apply(this, arguments));
  }

  _createClass(VirtualPlayer, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.isStart !== this.props.isStart || nextProps.playing !== this.props.playing || nextProps.backgroundImage !== this.props.backgroundImage || nextProps.imageWidth !== this.props.imageWidth) {
        return true;
      }
      return false;
    }
  }, {
    key: "renderIcon",
    value: function renderIcon() {
      var _props = this.props,
          playing = _props.playing,
          isStart = _props.isStart;

      if (playing) {
        return null;
      } else {
        if (!isStart) {
          return _react2.default.createElement(
            "div",
            null,
            _react2.default.createElement(
              "svg",
              { x: "0px", y: "0px", width: "40", height: "40", fill: "#FFF", viewBox: "0 0 512 512" },
              _react2.default.createElement(
                "g",
                null,
                _react2.default.createElement("path", { d: "M224,435.8V76.1c0-6.7-5.4-12.1-12.2-12.1h-71.6c-6.8,0-12.2,5.4-12.2,12.1v359.7c0,6.7,5.4,12.2,12.2,12.2h71.6 C218.6,448,224,442.6,224,435.8z" }),
                _react2.default.createElement("path", { d: "M371.8,64h-71.6c-6.7,0-12.2,5.4-12.2,12.1v359.7c0,6.7,5.4,12.2,12.2,12.2h71.6c6.7,0,12.2-5.4,12.2-12.2V76.1 C384,69.4,378.6,64,371.8,64z" })
              )
            )
          );
        } else {
          return _react2.default.createElement(
            "div",
            null,
            _react2.default.createElement(
              "svg",
              { x: "0px", y: "0px", width: "40", height: "40", fill: "#FFF", viewBox: "0 0 512 512" },
              _react2.default.createElement("path", { d: "M405.2,232.9L126.8,67.2c-3.4-2-6.9-3.2-10.9-3.2c-10.9,0-19.8,9-19.8,20H96v344h0.1c0,11,8.9,20,19.8,20 c4.1,0,7.5-1.4,11.2-3.4l278.1-165.5c6.6-5.5,10.8-13.8,10.8-23.1C416,246.7,411.8,238.5,405.2,232.9z" })
            )
          );
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _props2 = this.props,
          onPlay = _props2.onPlay,
          onPause = _props2.onPause,
          playing = _props2.playing,
          backgroundImage = _props2.backgroundImage,
          isStart = _props2.isStart,
          imageWidth = _props2.imageWidth,
          imageHeight = _props2.imageHeight,
          waterMarkText = _props2.waterMarkText,
          waterMarkLink = _props2.waterMarkLink,
          playerWidth = _props2.playerWidth,
          embed = _props2.embed;


      return _react2.default.createElement(
        "div",
        {
          className: "virtualPlayer",
          style: {
            backgroundImage: isStart && !playing ? "url(" + backgroundImage + ")" : "",
            backgroundSize: embed ? '100%' : imageWidth + "px " + imageHeight + "px"
          },
          onClick: function onClick() {
            return playing ? onPause() : onPlay();
          } },
        this.renderIcon(),
        !playing && embed ? _react2.default.createElement(
          "a",
          { href: waterMarkLink, target: "_blank", style: { fontSize: playerWidth * 26 / 853 } },
          waterMarkText.slice(0, -4),
          _react2.default.createElement(
            "span",
            { style: { fontSize: playerWidth * 18 / 853 } },
            waterMarkText.slice(-4)
          )
        ) : null
      );
    }
  }]);

  return VirtualPlayer;
}(_react.Component);

exports.default = VirtualPlayer;