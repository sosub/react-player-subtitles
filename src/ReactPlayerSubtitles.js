import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import screenfull from 'screenfull';

import ReactPlayer from 'react-player';
import VirtualPlayer from './components/VirtualPlayer';
import Subtitles from './components/Subtitles';
import Controls from './components/Controls';
import ScriptSide from './components/ScriptSide';

import { 
  getTextFileContent, 
  textContentToSubtitleData, 
  getCurrentSubtitle,
  setCookie, getCookie } from './utils';

class ReactPlayerSubtitles extends Component {
  constructor(props) {
    super(props);
    let volume = 1;
    let subtitleDisplayType = "both";
    let subtitleIsBottom = false;
    let showTranscript = false;

    // get values from cookie if enable
    if (props.cookieEnable) {
      volume = parseFloat(getCookie(`${props.cookiePrefix}.volume`, props.cookies)) || 1;
      subtitleDisplayType = getCookie(`${props.cookiePrefix}.subtitleDisplayType`, props.cookies) || "both";
      subtitleIsBottom = getCookie(`${props.cookiePrefix}.subtitleIsBottom`, props.cookies) === "true";
      showTranscript = getCookie(`${props.cookiePrefix}.showTranscript`, props.cookies) === "true";
    }

    const playerSize = this.getPlayerSize(false, showTranscript, props.scriptSideWidth, props.playerWidth);

    this.state = {
      duration: 0,
      currentTime: 0,
      playing: false,
      volume,
      playbackRate: 1,
      qualities: ["medium"],
      quality: "medium",
      primarySubtitleData: null,
      secondarySubtitleData: null,
      primarySubtitleDataList: [],
      secondarySubtitleDataList: [],
      currentPrimarySubtitle: "",
      currentSecondarySubtitle: "",
      subtitleDisplayType,
      subtitleIsBottom,
      showTranscript,
      isFullscreen: false,
      displayControls: true,
      playerWidth: playerSize.playerWidth,
      playerHeight: playerSize.playerHeight,
    }
  }

  componentWillMount() {
    // collect the subtitles at first time
    this.collectSubtitle(this.props.primarySubtitleURL, "primarySubtitleData");
    this.collectSubtitle(this.props.secondarySubtitleURL, "secondarySubtitleData");
    // event key down
    try {
      document.addEventListener("keydown", this._onKeyDown);
    } catch(e) {}
  }

  componentDidMount() { 
    // add fullscreen event (for escape key)
    if (screenfull.enabled) {
      try {
        document.addEventListener(screenfull.raw.fullscreenchange, this.exitFullscreenListener);
      } catch(e) {}
    }
  }

  componentWillReceiveProps(nextProps) {
    // collect the subtitles if change link
    if (this.props.primarySubtitleURL !== nextProps.primarySubtitleURL) {
      this.collectSubtitle(nextProps.primarySubtitleURL, "primarySubtitleData");
    }

    if (this.props.secondarySubtitleURL !== nextProps.secondarySubtitleURL) {
      this.collectSubtitle(nextProps.secondarySubtitleURL, "secondarySubtitleData");
    }

    // change size for responsive purpose (for wrapper app)
    if (this.props.playerWidth !== nextProps.playerWidth || this.props.playerHeight !== nextProps.playerHeight) {
      const playerSize = this.getPlayerSize(false, this.props.showTranscript, nextProps.scriptSideWidth, nextProps.playerWidth);
      this.setState({
        playerHeight: playerSize.playerHeight,
        playerWidth: playerSize.playerWidth,
      });
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (nextProps.primary !== this.props.primary 
  //     || nextProps.secondary !== this.props.secondary
  //     || nextProps.playerWidth !== this.props.playerWidth) {
  //     return true;
  //   }
  //   return false;
  // }

  // componentWillUpdate(nextProps, nextState) {
  // }

  componentDidUpdate(prevProps, prevState) {
    // turn off subtitle from youtube
    if (this.player && this.player.player.player && this.player.player.player.unloadModule) {
      this.player.player.player.unloadModule('captions');
      this.player.player.player.unloadModule('cc');
    }

    // set cookie if enable
    if(this.props.cookieEnable) {
      const cookiePrefix = this.props.cookiePrefix;
      const { volume, subtitleDisplayType, subtitleIsBottom, showTranscript } = this.state;
      if (volume !== prevState.volume) {
        setCookie(`${this.props.cookiePrefix}.volume`, volume, 60);
      }
      if (subtitleDisplayType !== prevState.subtitleDisplayType) {
        setCookie(`${cookiePrefix}.subtitleDisplayType`, subtitleDisplayType, 60);
      }
      if (subtitleIsBottom !== prevState.subtitleIsBottom) {
        setCookie(`${cookiePrefix}.subtitleIsBottom`, subtitleIsBottom, 60);
      }
      if (showTranscript !== prevState.showTranscript) {
        setCookie(`${cookiePrefix}.showTranscript`, showTranscript, 60);
      }
    }
  }

  componentWillUnmount() {
    try {
      document.removeEventListener("keydown", this._onKeyDown);
      document.removeEventListener(screenfull.raw.fullscreenchange, this.exitFullscreenListener);
    } catch (e) {}
  }

  // _onReady() {
  //   console.log(this.player.player.player.getAvailableQualityLevels());
  // }

  _onStart() {
    // // count at first time play video
    // this.props.onViewCount();
    // this.lastCountTime = new Date();
    this.setState({
      qualities: this.player.player.player.getAvailableQualityLevels(),
      quality: this.player.player.player.getPlaybackQuality(),
    });
  }

  _onPlay() {
    this.changePlaying(true);
    this.showControls();
  }

  _onPause() {
    this.changePlaying(false);
    this.showControls();
  }

  _onMouseMove() {
    this.showControls();
  }

  _onProgress(current) {
    // change currentTime state
    const currentTime = current.playedSeconds;
    if (currentTime) {
      // Add 0.5s for delaying
      this.changeCurrentTime(currentTime + 0.5);
    }
  }

  _onDuration(duration) {
    // change duration state
    this.setState({ duration });
  }

  _onSeek(time) {
    // change currentTime state and seek play
    const { duration } = this.state;
    this.changeCurrentTime(time);
    this.player.seekTo(time/duration);
  }

  _onFullscreen() {
    this.setState({ isFullscreen: true });
    if (this.props.onFullScreen) {
      this.props.onFullScreen();
    } else {
      screenfull.request(findDOMNode(this._player));
      setTimeout(() => this.changePlayerSize(true, this.state.showTranscript), 100);
    }
  }

  _offFullscreen() {
    this.setState({ isFullscreen: false });
    if (this.props.offFullScreen) {
      this.props.offFullScreen();
    } else {
      screenfull.exit();
      this.changePlayerSize(false, this.state.showTranscript);
    }
  }

  _onKeyDown = (e) => {
    if (e.keyCode === 32) {
      // Space
      if (this.state.playing) {
        this.changePlaying(false);
      } else {
        this.changePlaying(true);
      }
      e.preventDefault();
    } else if (e.keyCode === 37) {
      // Backward
      this.fast(-3);
    } else if (e.keyCode === 39) {
      // Forward
      this.fast(3);
    }
  };

  _onToggleTranscipt() {
    this.setState({ showTranscript: !this.state.showTranscript });
    this.changePlayerSize(this.state.isFullscreen, !this.state.showTranscript);
  }

  _onQuality(quality) {
    this.setState({ quality });
    this.player.player.player.setPlaybackQuality(quality);
  }

  render() {
    const { 
      backgroundImage,
      url, 
      scriptSideWidth,
      waterMarkText,
      waterMarkLink,
      embed,
    } = this.props;
    const { 
      duration,
      currentTime, 
      playing,
      volume,
      playbackRate,
      qualities,
      quality,
      currentPrimarySubtitle, 
      currentSecondarySubtitle,
      subtitleDisplayType,
      subtitleIsBottom,
      secondarySubtitleData,
      primarySubtitleDataList,
      showTranscript,
      isFullscreen,
      displayControls,
      playerWidth,
      playerHeight
    } = this.state;

    const widthVideo = playerWidth;
    const heightVideo = playerHeight;

    return (
      <div className="reactPlayerSubtitles" ref={ref => this._player = ref}>
        <div 
          className="reactPlayerSubtitles__player"
          style={{ height: embed ? '100vh' : heightVideo }}
          onMouseMove={() => this._onMouseMove()}>
          <ReactPlayer 
            ref={player => this.player = player}
            url={url} 
            width={widthVideo}
            height={heightVideo}
            playing={playing}
            volume={volume}
            // onReady={() => this._onReady()}
            playbackRate={playbackRate}
            onStart={() => this._onStart()}
            onPlay={() => this._onPlay()}
            onPause={() => this._onPause()}
            onProgress={current => this._onProgress(current)}
            onDuration={duration => this._onDuration(duration)}
          />
          <Subtitles
            primary={currentPrimarySubtitle}
            secondary={currentSecondarySubtitle}
            displayType={subtitleDisplayType}
            isBottom={subtitleIsBottom}
            displayControls={displayControls}
            playerWidth={playerWidth > playerHeight ? playerWidth : playerHeight}
          />
          <VirtualPlayer
            onPlay={() => this._onPlay()}
            onPause={() => this._onPause()}
            playing={playing}
            isStart={!currentTime}
            backgroundImage={backgroundImage}
            imageWidth={widthVideo}
            imageHeight={heightVideo}
            waterMarkText={waterMarkText}
            waterMarkLink={waterMarkLink}
            playerWidth={playerWidth > playerHeight ? playerWidth : playerHeight}
            embed={embed}
          />
          <Controls
            display={displayControls}
            duration={duration}
            currentTime={currentTime}
            playing={playing}
            onSeek={time => this._onSeek(time)}
            onPlay={() => this._onPlay()}
            onPause={() => this._onPause()}
            volume={volume}
            onVolume={volume => this.setState({ volume })}
            onToggleTranscipt={() => this._onToggleTranscipt()}
            onFullscreen={() => this._onFullscreen()}
            offFullscreen={() => this._offFullscreen()}
            isFullscreen={isFullscreen}
            onMouseEnter={() => this.setState({ mouseOnControls: true })}
            onMouseLeave={() => this.setState({ mouseOnControls: false })}
            subtitleDisplayType={subtitleDisplayType}
            subtitleIsBottom={subtitleIsBottom}
            changeDisplayType={(type) => this.setState({ subtitleDisplayType: type })}
            changeIsBottom={(bool) => this.setState({ subtitleIsBottom: bool })}
            playbackRate={playbackRate}
            onPlaybackRate={playbackRate => this.setState({ playbackRate })}
            qualities={qualities}
            quality={quality}
            onQuality={quality => this._onQuality(quality)}
          />
        </div>
        {
          showTranscript && typeof window !== 'undefined' && window.innerWidth > 768
          ?
          <ScriptSide
            height={playerHeight}
            width={scriptSideWidth}
            // primaryData={primarySubtitleData}
            primaryDataList={primarySubtitleDataList}
            secondaryData={secondarySubtitleData}
            // secondaryDataList={secondarySubtitleDataList}
            onClick={time => this._onSeek(time)}
            currentPrimary={currentPrimarySubtitle}
          />
          :
          null
        }
      </div>
    );
  }

  collectSubtitle(url, stateName) {
    getTextFileContent(url, text => {
      const subtitleData = textContentToSubtitleData(text);
      this.setState({ 
        [stateName]: subtitleData.data,
        [`${stateName}List`]: subtitleData.list,
      })
    });
  }

  changeCurrentTime(time) {
    // add change the current subtitles too for performance require
    const { primarySubtitleDataList, secondarySubtitleDataList } = this.state;
    this.setState({ 
      currentTime: time,
      currentPrimarySubtitle: getCurrentSubtitle(time, primarySubtitleDataList),
      currentSecondarySubtitle: getCurrentSubtitle(time, secondarySubtitleDataList),
    });
  }

  changePlaying(state) {
    // count video view if reach the conditions
    if (state) {
      const currentPercent = this.state.currentTime/(this.state.duration/100);
      if (!this.lastCountTime 
        || (new Date() - this.lastCountTime > this.props.viewCountTime*60000 && currentPercent < 2)) {
        this.props.onViewCount();
        this.lastCountTime = new Date();
      }
    }

    // change playing state
    this.setState({ playing: state });
  }

  showControls() {
    // show controls on 3s
    if (!this.state.displayControls) {
      this.setState({ displayControls: true });
    }
    clearTimeout(this._timeout);
    if (this.state.playing) {
      this._timeout = setTimeout(
        () => this.setState({ 
          displayControls: !this.state.playing || this.state.mouseOnControls ? true : false,
        }),
        800
      );
    }
  }

  fast(time) {
    this._onSeek(this.state.currentTime + time);
  }

  getPlayerSize(isFullscreen, showTranscript, scriptSideWidth, playerWidthInput) {
    let playerWidth = playerWidthInput;
    if (typeof window !== 'undefined') {
      if (showTranscript && window.innerWidth > 768 && playerWidth + scriptSideWidth > window.innerWidth) {
        playerWidth = window.innerWidth - scriptSideWidth;
      } else if (playerWidth > window.innerWidth) {
        playerWidth = window.innerWidth;
      }
    }
    
    let playerHeight = playerWidth * 480 / 853;
    if (typeof window !== 'undefined') {
      if (showTranscript && window.innerWidth > 768 && playerHeight < 350) {
        playerHeight = 350;
      }
    }
    
    if (typeof window !== 'undefined') {
      if (isFullscreen || this.props.embed) {
        playerWidth = window.innerWidth;
        playerHeight = window.innerHeight;
        if (showTranscript && window.innerWidth > 768) {
          playerWidth = window.innerWidth - scriptSideWidth;
        }
      }
    }
    
    return { playerHeight, playerWidth }
  }

  changePlayerSize(isFullscreen, showTranscript) {
    const playerSize = this.getPlayerSize(isFullscreen, showTranscript, this.props.scriptSideWidth, this.props.playerWidth);
    this.setState({
      playerHeight: playerSize.playerHeight,
      playerWidth: playerSize.playerWidth
    });
  }

  exitFullscreenListener = () => {
    if (!screenfull.isFullscreen) {
      this._offFullscreen();
    }
  }
}

ReactPlayerSubtitles.propTypes = {
  backgroundImage: PropTypes.string,
  url: PropTypes.string,
  playerWidth: PropTypes.number,
  scriptSideWidth: PropTypes.number,
  primarySubtitleURL: PropTypes.string,
  secondarySubtitleURL: PropTypes.string,
  primarySubtitleName: PropTypes.string,
  secondarySubtitleName: PropTypes.string,
  onViewCount: PropTypes.func,
  viewCountTime: PropTypes.number,
  onFullScreen: PropTypes.func,
  offFullScreen: PropTypes.func,
  embed: PropTypes.bool,
  waterMarkText: PropTypes.string,
  waterMarkLink: PropTypes.string,
}

ReactPlayerSubtitles.defaultProps = {
  backgroundImage: "https://sosub.org/media/video_images/image_lhQ4UY5.jpg",
  url: "https://www.youtube.com/watch?v=uFX95HahaUs",
  // playerWidth: '100%',
  playerWidth: 853,
  scriptSideWidth: 340,
  primarySubtitleURL: "/vi_Sg5M9yX_HA4qmn3.srt",
  secondarySubtitleURL: "/en_GkAJOX2_B9gswf1.srt",
  primarySubtitleName: "Tiếng Việt",
  secondarySubtitleName: "English",
  onViewCount: () => console.log("onViewCount"),
  viewCountTime: 3,
  cookiePrefix: "SOSUB",
  cookieEnable: true,
  embed: false,
  waterMarkText: "SOSUB.org",
  waterMarkLink: "SOSUB.org",
}

export default ReactPlayerSubtitles;
