import React, { Component } from 'react';

class Subtitles extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.primary !== this.props.primary 
      || nextProps.secondary !== this.props.secondary
      || nextProps.displayType !== this.props.displayType
      || nextProps.isBottom !== this.props.isBottom
      || nextProps.displayControls !== this.props.displayControls
      || nextProps.playerWidth !== this.props.playerWidth) {
      return true;
    }
    return false;
  }

  renderPrimaryType(text) {
    const fontSize = this.props.playerWidth*22/853;
    return (
      text &&
      <div className="primary">
        <span style={{ fontSize }}>{text}</span>
      </div> 
    );
  }

  renderSecondaryType(text) {
    const fontSize = this.props.playerWidth*18/853;
    return (
      text &&
      <div className="secondary">
        <span style={{ fontSize }}>{text}</span>
      </div> 
    );
  }

  render() {
    const { primary, secondary, displayType, isBottom, displayControls } = this.props;

    return (
      displayType !== "off"
      ?
      <div className="subtitles" style={{ bottom: displayControls ? 60 : 10 }}>
        {
          displayType === "both" && !isBottom 
          ? 
          <div className="top">
            {this.renderPrimaryType(secondary)}
          </div> 
          :
          null
        }
        {
          displayType === "both"
          ?
          <div className="bottom">
            {this.renderPrimaryType(primary)}
            {isBottom && this.renderSecondaryType(secondary)}
          </div>
          :
          <div className="bottom">
            {this.renderPrimaryType(displayType === "primary" ? primary : secondary)}
          </div>
        }
      </div>
      :
      null
    );
  }
}

export default Subtitles;
