import React, { Component } from 'react';
import { secondsToTimeString } from '../utils';

class ScriptSide extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onMouse: false,
    }
  }

  componentDidMount() {
    setTimeout(
      () => {
        this.scrollSubtitle(this.props.currentPrimary);
      },
      500
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.onMouse !== this.state.onMouse
        || nextProps.height !== this.props.height
        || nextProps.width !== this.props.width
        || nextProps.secondaryData !== this.props.secondaryData
        || nextProps.primaryDataList !== this.props.primaryDataList
        || nextProps.currentPrimary !== this.props.currentPrimary) {
      return true;
    }
    return false;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentPrimary !== this.props.currentPrimary && !this.state.onMouse) {
      this.scrollSubtitle(nextProps.currentPrimary);
    }
  }

  _onMouseEnter() {
    this.setState({ onMouse: true });
  }

  _onMouseLeave() {
    this.scrollSubtitle(this.props.currentPrimary);
    this.setState({ onMouse: false });
  }

  _onMouseMove() {
    if (!this.state.onMouse) {
      this.setState({ onMouse: true });
    }
  }

  scrollSubtitle(currentSubtitle) {
    try {
      const currentSubtitleNumber = this.props.primaryDataList.map(subtitle => subtitle.content).indexOf(currentSubtitle);
      const ul = document.getElementsByClassName('scriptSide__ul')[0];
      const ulOffsetTop = ul.offsetTop;
      const ulHeight = ul.offsetHeight;
      const currentLi = document.getElementsByClassName('scriptSide__ul__li')[currentSubtitleNumber-1]
      const liOffsetTop = currentLi.offsetTop;
      const liHeight = currentLi.offsetHeight;
      document.getElementsByClassName('scriptSide__ul')[0].scrollTop = liOffsetTop - ulOffsetTop + liHeight - ulHeight/2 + liHeight/2;
    } catch (e) {}
  }

  render() {
    const { height, width, secondaryData, primaryDataList, onClick, currentPrimary } = this.props;
    return (
      <div className="scriptSide" style={{ height, width }}>
        <ul 
          className="scriptSide__ul" 
          onMouseEnter={() => this._onMouseEnter()}
          onMouseLeave={() => this._onMouseLeave()}
          onMouseMove={() => this._onMouseMove()}>
          {primaryDataList.map(subtitle => {
            return (
              <li 
                key={subtitle.key} 
                className={`scriptSide__ul__li ${currentPrimary === subtitle.content ? "active" : ""}`}
                onClick={() => onClick(subtitle.begin + 0.1)}>
                <div>
                  <span>{secondsToTimeString(subtitle.begin)}</span>
                  <div>
                    <p className="primarySubtitle">
                      {subtitle.content}
                    </p>
                    <p className="secondarySubtitle">
                      {secondaryData && secondaryData[subtitle.key] ? secondaryData[subtitle.key].content : ""}
                    </p>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ScriptSide;
