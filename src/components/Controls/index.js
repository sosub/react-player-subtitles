import React, { Component } from 'react';
import ProgressBar from './ProgressBar';
import PlayAndPause from './PlayAndPause';
import VolumeButton from './VolumeButton';
import ScriptButton from './ScriptButton';
import Duration from './Duration';
import CaptionButton from './CaptionButton';
import ResizeButton from './ResizeButton';
import SettingsButton from './SettingsButton';

class Controls extends Component {
  render() {
    const { 
      display,
      onMouseEnter, onMouseLeave,
      duration, currentTime, onSeek, 
      playing, onPlay, onPause, 
      volume, onVolume, onToggleTranscipt, 
      onFullscreen, offFullscreen, isFullscreen,
      subtitleDisplayType, subtitleIsBottom, changeDisplayType, changeIsBottom,
      playbackRate, onPlaybackRate, quality, qualities, onQuality } = this.props;

    return (
      <div 
        className={`controls${!display ? ' hidden' : ''}`}
        onMouseEnter={() => onMouseEnter()} 
        onMouseLeave={() => onMouseLeave()}>
        <ProgressBar
          duration={duration}
          currentTime={currentTime}
          onChange={time => onSeek(time)}
        />
        <div className="items">
          <div className="leftItems">
            <PlayAndPause
              playing={playing}
              onPlay={() => onPlay()}
              onPause={() => onPause()}
            />
            <VolumeButton
              volume={volume}
              onVolume={volume => onVolume(volume)}
            />
            <Duration
              current={currentTime}
              duration={duration}
            />
          </div>
          <div className="rightItems">
            {
              typeof window !== 'undefined' && window.innerWidth > 768 &&
              <SettingsButton
                playbackRate={playbackRate}
                onPlaybackRate={(rate) => onPlaybackRate(rate)}
                quality={quality}
                qualities={qualities}
                onQuality={(quality) => onQuality(quality)}
              />
            }
            <CaptionButton
              subtitleDisplayType={subtitleDisplayType}
              subtitleIsBottom={subtitleIsBottom}
              changeDisplayType={(type) => changeDisplayType(type)}
              changeIsBottom={(bool) => changeIsBottom(bool)}
            />
            {
              typeof window !== 'undefined' && window.innerWidth > 768 &&
              <ScriptButton
                onClick={() => onToggleTranscipt()}
              />
            }
            <ResizeButton
              onFullscreen={() => onFullscreen()}
              offFullscreen={() => offFullscreen()}
              isFullscreen={isFullscreen}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Controls;
