import React, { Component } from 'react';
import { ProgressBar as RPCProgressbar } from 'react-player-controls';

export default class ProgressBar extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.currentTime !== this.props.currentTime
        || nextProps.duration !== this.props.duration) {
      return true;
    }
    return false;
  }
  
  render() {
    const { duration, currentTime, onChange } = this.props;

    return (
      <div className="progressBar">
        <RPCProgressbar
          totalTime={duration}
          currentTime={currentTime}
          isSeekable={true}
          onSeek={time => onChange(time)}
        />
      </div>
    );
  }
}