import React, { Component } from 'react';
import { VolumeSlider as RPCVolumeSlider } from 'react-player-controls';

export default class VolumeSlider extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.volume !== this.props.volume) {
      return true;
    }
    return false;
  }
  
  render() {
    const { volume, onChange } = this.props;

    return (
      <div className="volumeSlider">
        <RPCVolumeSlider
          volume={volume}
          isEnabled={true}
          onVolumeChange={volume => onChange(volume)}
        />
      </div>
    );
  }
}