import React, { Component } from 'react';

export default class TwoLayersMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayLayer: null,
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.data !== this.props.data
        || nextState.displayLayer !== this.state.displayLayer) {
      return true;
    }
    return false;
  }

  _onClick(layer) {
    this.setState({ displayLayer: layer });
  }

  render() {
    const data = this.props.data;
    const displayLayer = this.state.displayLayer;

    return (
      <table className="twoLayersMenu">
        {
          displayLayer === null
          ?
          <tbody>
            {data.map((layer, idx) => 
              <tr key={idx} onClick={() => this._onClick(layer)}>
                <td><label>{layer.label}</label></td>
                <td><b>{layer.value}</b></td>
                <td>
                  <svg x="0px" y="0px" width="14" height="14" fill="#1c2029" viewBox="0 0 512 512" >
                    <path d="M298.3,256L298.3,256L298.3,256L131.1,81.9c-4.2-4.3-4.1-11.4,0.2-15.8l29.9-30.6c4.3-4.4,11.3-4.5,15.5-0.2l204.2,212.7
                      c2.2,2.2,3.2,5.2,3,8.1c0.1,3-0.9,5.9-3,8.1L176.7,476.8c-4.2,4.3-11.2,4.2-15.5-0.2L131.3,446c-4.3-4.4-4.4-11.5-0.2-15.8
                      L298.3,256z"/>
                  </svg>
                </td>
              </tr>
            )}
          </tbody>
          :
          <tbody>
            <tr onClick={() => this._onClick(null)}>
              <td>
                <svg x="0px" y="0px" width="12" height="12" fill="#6e727d" viewBox="0 0 512 512" >
                  <path d="M213.7,256L213.7,256L213.7,256L380.9,81.9c4.2-4.3,4.1-11.4-0.2-15.8l-29.9-30.6c-4.3-4.4-11.3-4.5-15.5-0.2L131.1,247.9
                    c-2.2,2.2-3.2,5.2-3,8.1c-0.1,3,0.9,5.9,3,8.1l204.2,212.7c4.2,4.3,11.2,4.2,15.5-0.2l29.9-30.6c4.3-4.4,4.4-11.5,0.2-15.8
                    L213.7,256z"/>
                </svg>
              </td>
              <td><label>Trở lại {displayLayer.label}</label></td>
            </tr>
            {displayLayer.items.map((item, idx) => 
              <tr 
                key={idx} 
                onClick={() => {item.action();this._onClick(null);}}
                className={item.value === displayLayer.value ? "active" : null}>
                <td><label></label></td>
                <td><b>{item.value}</b></td>
              </tr>
            )}
          </tbody>
        }
      </table>
    );
  }
}