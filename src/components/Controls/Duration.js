import React, { Component } from 'react';
import { secondsToTimeString } from '../../utils';

export default class Duration extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.current !== this.props.current
        || nextProps.duration !== this.props.duration) {
      return true;
    }
    return false;
  }

  render() {
    const { current, duration } = this.props;

    return (
      <span className="duration">
        {`${secondsToTimeString(current)} / ${secondsToTimeString(duration)}`}
      </span>
    );
  }
}