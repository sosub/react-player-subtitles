import React, { Component } from 'react';

export default class ResizeButton extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.isFullscreen !== this.props.isFullscreen) {
      return true;
    }
    return false;
  }
  
  render() {
    const { onFullscreen, offFullscreen, isFullscreen } = this.props;

    return (
      isFullscreen
      ?
      <button className="resizeButton" onClick={() => offFullscreen()}>
        <svg x="0px" y="0px" width="20" height="20" fill="#E33734" viewBox="0 0 512 512" >
          <g id="check-box-outline-blank">
            <path d="M408,51v357H51V51H408 M408,0H51C22.95,0,0,22.95,0,51v357c0,28.05,22.95,51,51,51h357c28.05,0,51-22.95,51-51V51
              C459,22.95,436.05,0,408,0L408,0z"/>
          </g>
        </svg>
      </button>
      :
      <button className="resizeButton" onClick={() => onFullscreen()}>
        <svg x="0px" y="0px" width="20" height="20" fill="#E33734" viewBox="0 0 512 512" >
          <polygon points="288,96 337.9,145.9 274,209.7 274,209.7 145.9,337.9 96,288 96,416 224,416 174.1,366.1 357.4,182.9 366.1,174.1 
            416,224 416,96 "/>
        </svg>
      </button>
    );
  }
}