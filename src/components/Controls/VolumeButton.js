import React, { Component } from 'react';
import VolumeSlider from './VolumeSlider';

export default class VolumeButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMute: props.volume === 0,
      volume: props.volume,
      showVolumeSlider: false,
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.isMute !== this.state.isMute
        || nextState.volume !== this.state.volume
        || nextState.showVolumeSlider !== this.state.showVolumeSlider) {
      return true;
    }
    return false;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.volume !== this.props.volume) {
      this.setState({ isMute: nextProps.volume === 0 });
      if (nextProps.volume !== 0) {
        this.setState({ volume: nextProps.volume });
      }
    }
  }

  _on() {
    this.props.onVolume(this.state.volume);
  }

  _off() {
    this.props.onVolume(0);
  }

  _onMouseEnter() {
    this.setState({ showVolumeSlider: true });
  }

  _onMouseLeave() {
    this.setState({ showVolumeSlider: false });
  }

  render() {
    const { isMute, volume, showVolumeSlider } = this.state;
    const { onVolume } = this.props;
    return (
      <div className="volumeButton" onMouseEnter={() => this._onMouseEnter()} onMouseLeave={() => this._onMouseLeave()}>
      {
        isMute
        ?
        <button onClick={() => this._on()}>
          <svg x="0px" y="0px" width="20" height="20" fill="red" viewBox="0 0 512 512" >
            <g>
              <path d="M405.5,256c0,22.717-4.883,44.362-13.603,63.855l31.88,31.88C439.283,323.33,448,290.653,448,256
                c0-93.256-64-172.254-149-192v44.978C361,127.632,405.5,186.882,405.5,256z"/>
              <polygon points="256,80.458 204.979,132.938 256,183.957   "/>
              <path d="M420.842,396.885L91.116,67.157l-24,24l90.499,90.413l-8.28,10.43H64v128h85.334L256,431.543V280l94.915,94.686
                C335.795,387.443,318,397.213,299,403.022V448c31-7.172,58.996-22.163,82.315-42.809l39.61,39.693l24-24.043l-24.002-24.039
                L420.842,396.885z"/>
              <path d="M352.188,256c0-38.399-21.188-72.407-53.188-88.863v59.82l50.801,50.801C351.355,270.739,352.188,263.454,352.188,256z"/>
            </g>
          </svg>
        </button>
        :
        (
          volume > 0.5
          ?
          <button onClick={() => this._off()}>
            <svg x="0px" y="0px" width="20" height="20" fill="red" viewBox="0 0 512 512" >
              <path d="M64,192v128h85.334L256,431.543V80.458L149.334,192H64z M352,256c0-38.399-21.333-72.407-53.333-88.863v176.636
                C330.667,328.408,352,294.4,352,256z M298.667,64v44.978C360.531,127.632,405.334,186.882,405.334,256
                c0,69.119-44.803,128.369-106.667,147.022V448C384,428.254,448,349.257,448,256C448,162.744,384,83.746,298.667,64z"/>
            </svg>
          </button>
          :
          <button onClick={() => this._off()}>
            <svg x="0px" y="0px" width="20" height="20" fill="red" viewBox="0 0 512 512" >
              <path d="M64,192v128h85.334L256,431.543V80.458L149.334,192H64z M352,256c0-38.399-21.333-72.407-53.333-88.863v176.636
                C330.667,328.408,352,294.4,352,256z"/>
            </svg>
          </button>
       )
      }
      {
        showVolumeSlider
        ?
        <VolumeSlider
          volume={volume}
          onChange={volume => onVolume(volume)}
        />
        :
        null
      }
      </div>
    );
  }
}