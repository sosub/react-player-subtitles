import React, { Component } from 'react';

export default class ScriptButton extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.onClick !== this.props.onClick) {
      return true;
    }
    return false;
  }
  
  render() {
    const { onClick } = this.props;

    return (
      <button onClick={() => onClick()}>
        <svg x="0px" y="0px" width="20" height="20" fill="#E33734" viewBox="0 0 512 512" >
          <g>
            <path d="M64,64v384h384V64H64z M144,368c-8.836,0-16-7.164-16-16s7.164-16,16-16s16,7.164,16,16S152.836,368,144,368z M144,272
              c-8.836,0-16-7.164-16-16s7.164-16,16-16s16,7.164,16,16S152.836,272,144,272z M144,176c-8.836,0-16-7.164-16-16s7.164-16,16-16
              s16,7.164,16,16S152.836,176,144,176z M384,360H192v-16h192V360z M384,264H192v-16h192V264z M384,168H192v-16h192V168z"/>
          </g>
        </svg>
      </button>
    );
  }
}