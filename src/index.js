import React from 'react';
import ReactDOM from 'react-dom';
import ReactPlayerSubtitles from './ReactPlayerSubtitles';
import './index.css';

ReactDOM.render(<ReactPlayerSubtitles embed/>, document.getElementById('root'));
