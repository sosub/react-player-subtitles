export function generateTimelineFrame(videoTime, videoProgressBarWidth) {
  // 62s & 853px -> [0 -> 62].length = 853 -> [key(pixel): value(second)]
  return [0].concat(Array.apply(0, new Array(videoProgressBarWidth)).map((i, idx) => Math.floor((idx+1)*videoTime/videoProgressBarWidth)));
}

export function secondsToTimeString(secondsTotal) {
  // 62.123s -> 1:02
  const total = parseInt(secondsTotal, 10);
  // const fraction = Math.ceil(((secondsTotal < 1.0) ? secondsTotal : (secondsTotal % Math.floor(secondsTotal))) * 10000);
  const seconds = total%60;
  const minutes = (total-seconds)/60%60;
  const hours = (total-seconds-minutes*60)/3600;
  return hours ? (hours+':'+zeroPad(minutes, 10)+':'+zeroPad(seconds, 10)) : (minutes+':'+zeroPad(seconds, 10))
}

export function zeroPad(nr, base) {
  // 1 -> 01
  var  len = (String(base).length - String(nr).length)+1;
  return len > 0? new Array(len).join('0')+nr : nr;
}

export function timeStrToSeconds(timeStr) {
  // 00:01:16,740 -> 76.740
  const time = timeStr.split(":");
  return parseInt(time[0], 10)*3600 + parseInt(time[1], 10)*60 + parseFloat(time[2].replace(',', '.'));
}

export function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

export function getCookie(cname, cookies) {
  var name = cname + "=";
  var ca = [];
  if (cookies) {
    ca = cookies.split(';');
  } else {
    if (typeof document !== 'undefined') {
      ca = document.cookie.split(';');
    }
  }
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)===' ') c = c.substring(1);
    if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
  }
  return "";
}

export function getTextFileContent(url, callback) {
  fetch(url)
    .then(response => {
      if (response.ok) {
        return response.text()
      }
    })
    .then(text => callback(text));
}

export function textContentToSubtitleData(textContent) {
  let endLineType = '\n';
  if (textContent.indexOf('\n\n') === -1) {
    endLineType = '\r';
  }
  let sentences = textContent.trim().split('\n'+endLineType);
  let sentencesObj = {};
  sentences = sentences.map(sentence => {
    sentence = sentence.split(endLineType);
    const content = sentence.slice(2).join(" ").trim();
    const time = sentence[1].split(' --> ');
    let result = {
      key: time[0].slice(0,11).trim(),
      begin: timeStrToSeconds(time[0]),
      end: timeStrToSeconds(time[1]),
      content
    };
    sentencesObj[time[0].slice(0,11).trim()] = result;
    return result;
  });

  return {
    data: sentencesObj,
    list: sentences,
  };
}

export function getCurrentSubtitle(currentTime, subtitleDataList) {
  if (subtitleDataList.length > 2) {
    if (currentTime < subtitleDataList[0].begin) {
      return '';
    }
    if (currentTime >= subtitleDataList[subtitleDataList.length-1].begin) {
      return subtitleDataList[subtitleDataList.length-1].content;
    }

    for (let i = 0;i < subtitleDataList.length;i++) {
      if (subtitleDataList[i].begin >= currentTime) {
        return subtitleDataList[i-1].content;
      }
    }
  } else {
    return '';
  }
}
